# Etiqa TripCare360 Web Application

The application is built as a VueJS single page application (SPA), then wrapped into Spring Boot to be packaged as a war application.

## Development
`vue-src` is the actual workspace. Refer to `vue-src/README.md` for further understanding.
