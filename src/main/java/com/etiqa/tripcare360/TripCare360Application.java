package com.etiqa.tripcare360;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TripCare360Application {

	public static void main(String[] args) {
		SpringApplication.run(TripCare360Application.class, args);
	}

}
