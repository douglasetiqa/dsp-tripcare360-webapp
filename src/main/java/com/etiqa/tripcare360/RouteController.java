package com.etiqa.tripcare360;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RouteController {

	/**
	 * Forward all the routing to Vue Router.
	 */
	@GetMapping(value = { "/{regex:\\w+}", "/**/{regex:\\w+}" })
	public String redirect() {
		return "forward:/";
	}

}
