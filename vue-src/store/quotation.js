export default {
  state: () => ({
    companyType: '',
    travelAreaType: '',
    travellerType: '',
    tripType: '',
    travelStartDate: '',
    travelEndDate: '',
    travelDuration: '',
    discountAmount: '',
    policyPremium: '',
    grossPremium: '',
    grossPremiumGst: '',
    grossPremiumStampDuty: '',
    totalPremiumPaid: '',
    agentCode: '',
    languageCode: '',
    offeredPlanName: '',
    discountPercent: '',
    isAdventurousAddonSelected: false
  }),
  mutations: {
    reset(state) {
      state.companyType = null;
      state.travelAreaType = null;
      state.travellerType = null;
      state.tripType = null;
      state.travelStartDate = null;
      state.travelEndDate = null;
      state.travelDuration = null;
      state.discountAmount = null;
      state.policyPremium = null;
      state.grossPremium = null;
      state.grossPremiumGst = null;
      state.grossPremiumStampDuty = null;
      state.totalPremiumPaid = null;
      state.agentCode = null;
      state.languageCode = null;
      state.offeredPlanName = null;
      state.discountPercent = null;
      state.isAdventurousAddonSelected = null;
    },
    update(state, props) {
      Object.entries(props).forEach(([k, v]) => {
        state[k] = v;
      });
    }
  }
};
