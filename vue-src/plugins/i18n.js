import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

export default ({ app }) => {
  app.i18n = new VueI18n({
    locale: window.localStorage.getItem('language'),
    fallbackLocale: 'en',
    messages: {
      en: require('~/locales/en.json'),
      ms: require('~/locales/ms.json')
    }
  })

  // Hydrate localStorage if it has no value.
  if (!window.localStorage.getItem('language')) {
    window.localStorage.setItem('language', app.i18n.fallbackLocale)
  }
}
